# Projet 3

---
Concepteur: Christophe DUFOUR  
Contexte: formation devops AJC/Luminess
___

## Problématique
L'entreprise Nuvola dispose actuellement d'une application symfony (php) - non conteneurisée - hébergée sur un seul serveur.  
Ce serveur, bien que puissant, connaît régulièrement des problèmes de saturation.  
Nuvola souhaite profiter des fonctionnalités de mise à échelle et d'équilibrage de charge que Kubernetes offre nativement.  
[Dépôt github de l'application](https://github.com/cdufour/symfony-120922-demo1)

## Votre mission
Conteneuriser l'application symfony, construire une image et publier l'image dans le dockerhub.  
Déployer l'application dans un cluster kubernetes.
Aide pour la "dockerization" de l'app symfony: https://youtu.be/KFWnB5hW6j8  
L'application symfony est servie par un serveur apache couplé à l'interpréteur PHP (version 7).   
Il est donc conseillé, dans le Dockerfile, de partir d'une image incluant apache et php.  

L'application symfony devra communiquer avec un serveur de base de données mariadb/mysql.  
Ce serveur de bdd sera également déployé dans le cluster.

A l'intérieur de cluster, créer une tâche (voir la ressource [Cronjob](https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/)) qui devra produire un backup de la base de données toutes les * * * * * (mettre quelques minutes en phase de test).  
Le fichier de backup devra être envoyé dans un compartement (bucket) AWS.  

## Technogies utiles
Docker, Kubernetes, SQL

## Livrables attendus
- Dockerfile de l'application symfony
- Manifestes yaml des ressources à déployer dans le cluster

## Notes techniques

### Image de l'application symfony
Si vous avez des difficultés à constuire l'image docker de l'application, vous pouvez utiliser  
cette-ci: *opusidea/luminess-projet3:0.2* depuis votre manifeste kubernetes de déploiement.  

### Base de données
Pour intéragir avec un serveur de base de données, l'application symfony se sert de   
la variable d'environnment *DATABASE_URL* située dans le fichier .env  
Cette variable peut être redéfinie dans le manifeste kubernetes de déploiement de l'application.  

  
DATABASE_URL="mysql://*utilisateur*:*motdepasse*@*ip_ou_nom_serveur*:*port_serveur*/*nom_base*?serverVersion=*version_serveur*"

  
Exemples de valeurs pouvant prendre cette variable d'environnement, selon votre paramètrage: 
- DATABASE_URL="mysql://root:stagiaire@127.0.0.1:3396/formation?serverVersion=mariadb-13.3.36"
- DATABASE_URL="mysql://root:toto@mysql-svc:3306/formation?serverVersion=5.7"

Pour tester que votre pod symfony a bien accès au pod mysql/mariadb:
```bash
kubectl exec -it <pod_name> -- bash

# Dans le répertoire de l'application, exécuter: 
php bin/console doctrine:database:create
```

Pour reconstituer le schéma de base de données (création des tables en "mapping" avec les entités de l'application)  
```bash
kubectl exec -it <pod_name> -- bash

# Dans le répertoire de l'application, exécuter: 
php bin/console doctrine:migrations:migrate
```

### Routes/endpoints de l'application Symfony
La quasi totalité des routes exposées par l'application se trouve sous forme d'une annotation *@Route* dans les fichiers  
xxxController.php situés dans le dossier src/Controller/  

Par exemple, le fichier src/Controller/DemoController, expose une route */demo2* associée à une fonction.    
Cette fonction est exécutée quand une requête est reçue par l'application sur la route */demo2*  
```php
     /**
     * @Route("/demo2", name="demo2")
     */
    public function demo2(): Response
    {
        $res = new Response();
        $content = "<h1>Demo2</h1>";
        $content .= "<p>Simple paragraphe</p>";
        $res->setContent($content);
        return $res;
    }
```
Une simple réponse http est alors retournée au client.    
Ci dessous, capture d'écran:    
<img src="scr/route.png" with="200px" />

#### Autre exemple
La route /student_form renvoie le formulaire html suivant:
<img src="scr/student_form.png" with="200px" />  

En se connectant directement au pod de base de données et en exécutant le client mysql embarqué (mysql -p)
On peut vérifier que les informations saisies par le client se retrouvent bien en base:  
<img src="scr/mysqlcli.png" with="200px" />  


