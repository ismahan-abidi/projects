# Exo 1

Déployer dans le cluster (minikube) l'application nodejs dont voici les sources:
https://gitlab.com/cdufour1/nodeapp

Il s'agit d'un serveur web écoutant le port 3000  
Prévoir une connectivité externe pour atteindre ce serveur depuis l'extérieur du cluster