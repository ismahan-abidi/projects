# 2022-devops-ajc-luminess


## Environnement virtualisé
Hyperviseur à installer sur votre système: *virtualbox*  
Lien: https://www.virtualbox.org/


Créer une machine virtuelle, avec ou sans environnement graphique (selon vos préférences):

### Installation d'une VM ubuntu 22.04 Desktop sur Virtualbox
[Vidéo - 45 min - format .webm (chrome)](https://opusidea-training.s3.eu-west-3.amazonaws.com/divers/2022-08-01-installation-ubuntu-vb.webm)

### Installation d'une VM ubuntu 22.04 Server sur Virtualbox
[Vidéo youtube - 7 min](https://youtu.be/vxb894qV7-8)


### Installation de docker sur ubuntu
```bash

# installation du paquet
sudo apt install -y docker.io

# création du groupe docker
sudo groupadd docker

# ajout de l'utilisateur courant au groupe
sudo usermod -aG docker $USER

# activiation immédiate du changement de groupe
newgrp docker

# affichage de la version de docker
docker version
```
