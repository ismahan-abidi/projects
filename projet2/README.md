# Projet 2

---
Concepteur: Christophe DUFOUR  
Contexte: formation devops AJC/Luminess
___

## Présentation
L'entreprise Nuvola, start-up ambitieuse, est en train de développer une application nommée *nuvolapp* 
Il s'agit d'une application web réalisée en nodejs/mongodb. 
Cette application devra offrir au client la possibilité d'enregistrer ses dépenses.   
Code source de l'application: https://github.com/cdufour/nuvolapp  

Nuvola souhaite développer rapidement de nouvelles fonctionnalités, et, dans cette optique, elle prévoit de recruter des développeurs supplémentaires. 
Cependant, ne disposant pas de locaux physiques pour accueillir les développeurs, 
elle souhaite leur fournir des machines virtuelles déjà pré-configurées.

## Votre mission chez Nuvola comporte deux tâches principales:

### Approvisionnement et configuration d'une machine de dev
Permettre de fournir rapidement une machine virtuelle à un développeur intégrant le projet.  
Cette machine devra être approvisionnée chez AWS (ou chez un autre cloud provider, ou en réseau privé local si non applicable).  
Le développeur pourra se connecter à sa machine par ssh grâce à une clé rsa privée que vous pourrez lui transmettre.  
L'administrateur pourra contrôler les machines de dev afin d'exécuter des tâches (installation, copie de fichiers, création de compte utilisateur, etc.)   
Vous êtes libre quant au choix de l'OS des machines de dev.  
La machine de dev devra disposer d'un compte utilisateur nommé selon le prénom de l'utilisateur, bas de casse, sans accent. Exemple: Jean-Rachid => *jean-rachid*.  
Le hostname de la machine devra être nommée *nuvola*.  
Ainsi, si Jean-Rachid se connecte à sa VM, son bash devra lui apparaître comme suit:  
*jean-rachid@nuvola:$*  

Cette machine devra disposer d'un groupe *developers*  
L'utilisateur devra appartenir au groupe *developers* ainsi qu'au groupe *docker*   
Une machine de dev devra disposer de:   
- copie du dépôt local de l'application nuvolapp (à placer dans le home directory de l'utilisateur).  
L'utilisateur pourra "pousser" des _commits_ sur le dépôt.
- nodejs
- npm
- docker
- images docker suivantes: node:14-alpine, mongo:4.4

### Conteneurisation et déploiement de l'application en continue
A terme, Nuvola souhaiterait déployer son application en cluster Kubernetes. 
Une version docker de l'application nuvoapp est donc nécessaire.  
Vous aurez en charge la conteneurisation (Docker) de l'application. 

Souhaitant appliquer les principes du Devops, Nuvola vous charge également d'automatiser au maximum
le processus de (re)déploiement. 
Le choix s'est porté sur Jenkins comme serveur d'automatisation. 
Vous devrez construire la pipeline permettant de: 
- récupérer le code source
- construire une nouvelle image docker sur la base de cette code source
- tester le lancement de l'application
- appliquer les tests unitaires (en option, si fournis)
- publier la nouvelle image sur un dépôt (registry) public ou privé. Cette image pourra être utilisée, ultérieurement, dans le contexte de production (ex: cluster Kubernetes)

Toutes les 3 heures (choisir une fréquence moindre durant la phase de test) la pipeline devra se redéclencher afin de rebuilder l'application en cas de changement sur le dépôt.  

![Schéma](scr/nuvola-schema.png)

## Notes

### Livrables attendus
- Si applicable: script ou playbook ansible ou manifest Puppet de création/lancement d'une machine de dev (machine virtuelle locale ou instance cloud)
- Playbook ansible ou manifest Puppet de configuration/approvisionnement d'une machine de dev
- Dockerfile de l'application nuvolapp
- Lien vers votre repository d'image docker (par exemple, vers le docker hub)
- Jenkinsfile de la pipeline d'intégration continue

### Technogies utiles
Shell, (AWS), Docker, Jenkins, Ansible/Puppet


### En option, s'il vous reste du temps
- Ajouter un étape de déploiement dans la pipeline Jenkins.   
L'environnement de déploiement sera une autre machine (réseau local privé ou cloud).   
Cette machine devra disposer du démon Docker pour instancier l'image de l'application dans sa dernière version.  
- Ajouter une étape de test post-déploiement dans la pipeline Jenkins. 
Ce test consistera en une requête http sur un endpoint de l'application en production afin de vérifier la réponse du serveur.  
- Ajouter une étape d'analyse de code statique dans la pipeline Jenkins.  
Cette étape se fera avant le build de l'image. On utilisera le module python *njsscan* pour analyser le fichier server.js  
Voir: https://github.com/ajinabraham/njsscan
![njsscan](scr/njsscan.png)
- Ajouter une étape d'analyse de qualité de code dans la pipeline Jenkins.  
Cette étape se fera avant le build de l'image. On utilisera le module nodejs *eslint*.  
Le fichier de configuration *my_eslint_config.yaml* est situé à la racine du dépôt de l'application nuvolapp.  
Cette simple commande permettra de tester la qualité du code:  
```bash
# l'option --yes permet de confirmer automatiquement la demande d'installation du package eslint
# l'option -c permet d'indiquer les fichier de configuration (règles syntaxiques) à prendre en compte
 npx --yes eslint -c my_eslint_config.yaml server.js 
```
![njsscan](scr/eslint.png)