# Exo 2

Créer un petit programme en langage C qui affiche: Hello From C !  
Placer le fichier source de ce programme (hello.c) sur un dépôt github/gitlab public ou privé.
Pour vos aider au niveau du paramètrage des credentials pour git, voir cette vidéo: https://youtu.be/HSA_mZoADSw  

Créer une pipeline Jenkins à 3 phases:
- clonage du dépôt
- compilation des sources (avec le compilateur gcc)
- exécution du programme

En option, ajouter cette phase:
- déploiement: le programme exécutable sera "poussé" dans un compartiment s3 (bucket)