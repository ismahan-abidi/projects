provider "aws" {
  region                   = "eu-west-2"
  shared_config_files      = ["C:/Users/chris/.aws/config"]
  shared_credentials_files = ["C:/Users/chris/.aws/credentials"]
  profile                  = "ajc"
}

resource "aws_iam_group" "trainees" {
  name = "group-chris-duf"
}

resource "aws_iam_user" "user1" {
  name = "user-chris-duf-1"
  tags = {
    tag-key = "Formation Terraform Luminess"
  }
}

resource "aws_iam_user" "user2" {
  name = "user-chris-duf-2"
  tags = {
    tag-key = "Formation Terraform Luminess"
  }
}

resource "aws_iam_group_membership" "team" {
  name = "trainees-to-team"

  users = [
    aws_iam_user.user1.name,
    aws_iam_user.user2.name
  ]

  group = aws_iam_group.trainees.name
}

# terraform import aws_iam_group.gp-marco marco
// resource "aws_iam_group" "gp-marco" {
//   name = "marco"
// }

// resource "aws_iam_group_membership" "marco-member" {
//   name = "marco-member"

//   users = [
//     aws_iam_user.user1.name
//   ]

//   group = aws_iam_group.gp-marco.name
// }
