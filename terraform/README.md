# Module Terraform

## Démarrage
[Install Terraform](https://developer.hashicorp.com/terraform/downloads)
[AWS CLI](https://aws.amazon.com/fr/cli/)

```bash
# télécharger terraform et awscli
# mettre à jour votre variable PATH en lui ajoutant l'emplacement des ces exécutables

# affichage de la version de terraform
terraform version

# affichage de la verison d'awscli
aws --version

# configuration d'awscli (génère ou modifie les fichiers .aws/credentials et .aws/config)
aws configure
```

#### Astuce
Si vous utilisez l'éditeur VSCode, installer l'extension "HCL" d'HashiCorp.    
https://marketplace.visualstudio.com/items?itemName=HashiCorp.HCL

## Liens utiles
- [Slides formateur](https://opusidea-training.s3.eu-west-3.amazonaws.com/presentation/ajc/Terraform.pdf)
- [8 Terraform Best Practices that will improve your TF workflow immediately - youtube - EN](https://youtu.be/gxPykhPxRW0)
- [Démo Modules Terraform - vidéo formateur](https://opusidea-training.s3.eu-west-3.amazonaws.com/divers/demo/2022-12-09-terraform-modules-demo.webm)