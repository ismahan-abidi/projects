variable "keyname" {
  type = string
  //default = "keyp-chrisd"
}

variable "musketeers" {
  type = list(string)
}


// resource "aws_s3_bucket" "b" {
//     bucket = "formation-terraform-luminess-christophe-dufour-2"
//     tags = {
//         Name        = "My bucket"
//         Environment = "Dev"
//     }
// }

// resource "aws_s3_bucket" "bb" {
//     bucket = "formation-terraform-luminess-christophe-dufour-3"

//     tags = {
//         Name        = "My bucket"
//         Environment = "Dev"
//     }
// }

resource "aws_key_pair" "keyp" {
  key_name   = var.keyname # référence à une variable
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD3F6tyPEFEzV0LX3X8BsXdMsQz1x2cEikKDEY0aIj41qgxMCP/iteneqXSIFZBp5vizPvaoIR3Um9xK7PGoW8giupGn+EPuxIA4cDM4vzOqOkiMPhz5XK0whEjkVzTo4+S0puvDZuwIsdiW9mxhJc7tgBNL0cYlWSYVkz4G/fslNfRPW5mYAM49f4fhtxPb5ok4Q2Lg9dPKVHO/Bgeu5woMc7RY0p1ej6D4CKFE6lymSDJpW0YHX/wqE9+cfEauh7xZcG0q9t2ta6F6fmX0agvpFyZo8aFbXeUBr7osSCJNgvavWbM/06niWrOvYX2xwWdhXmXSrbX8ZbabVohBK41 email@example.com"
}

resource "aws_instance" "web" {
  // ami locator: https://cloud-images.ubuntu.com/locator/ec2/
  ami           = "ami-01b8d743224353ffe" // ubuntu 22.04 london
  instance_type = "t3.micro"
  key_name      = aws_key_pair.keyp.key_name

  tags = {
    Name = "Chris Il Formatore"
    Keyname = var.keyname
    Musketeer = var.musketeers[0]
  }
}