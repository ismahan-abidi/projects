terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.45.0"
    }
  }
}

provider "aws" {
  region                   = "eu-west-2" // London
  shared_config_files      = ["C:/Users/chris/.aws/config"]
  shared_credentials_files = ["C:/Users/chris/.aws/credentials"]
  profile                  = "ajc"
}

module "webapp_1" {
  source = "../webapp"
  ami               = "ami-071f6f7c386495dc0"
  instance_type     = "t2.micro"
  tag_name          = "demo_module_1"
}

module "webapp_2" {
  source = "../webapp"
  ami               = "ami-071f6f7c386495dc0"
  instance_type     = "t3.micro"
  tag_name          = "demo_module_2"
}

output "out" {
  value = {
    wa1 = module.webapp_1
    wa2 = module.webapp_2
  }
}