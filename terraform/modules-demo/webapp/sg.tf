data "aws_vpc" "default_vpc" {
  default = true
}

resource "aws_security_group" "sg" {
    name        = var.tag_name
    description = var.tag_name
    vpc_id      = data.aws_vpc.default_vpc.id

    ingress {
        description      = "HTTP"
        from_port        = 80
        to_port          = 80
        protocol         = "tcp"
        cidr_blocks      = ["0.0.0.0/0"]
    }

    egress {
        from_port        = 0
        to_port          = 0
        protocol         = "-1"
        cidr_blocks      = ["0.0.0.0/0"]
    }

    tags = { Name = var.tag_name }
}
