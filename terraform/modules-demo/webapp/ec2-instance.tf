resource "aws_instance" "instance1" {
    ami = var.ami
    instance_type = var.instance_type
    vpc_security_group_ids = [aws_security_group.sg.id]
    tags = { Name = var.tag_name }
}