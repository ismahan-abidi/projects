output "instance_public_ip" {
   value = aws_instance.instance1.public_ip
}

output "sg_id" {
   value = aws_security_group.sg.id
}