# 1 VPC
resource "aws_vpc" "infra1" {
    cidr_block = "10.0.0.0/16"
    enable_dns_hostnames = true
    
    tags = {
        Name = var.tag_name
    }
}


# 2 Internet Gateway
resource "aws_internet_gateway" "infra1" {
    vpc_id = aws_vpc.infra1.id
    
    tags = {
        Name = var.tag_name
    }
}


# 3 Route table
resource "aws_route_table" "infra1" {
    vpc_id = aws_vpc.infra1.id

    route {
        cidr_block = "0.0.0.0/0" # IPv4
        gateway_id = aws_internet_gateway.infra1.id
    }

    tags = {
        Name = var.tag_name
    }
}


# 4 Public subnets
# Subnet 1
resource "aws_subnet" "infra1_subnet1" {
    vpc_id = aws_vpc.infra1.id
    cidr_block = "10.0.1.0/24"
    availability_zone = "${var.region}a"

    tags = {
        Name = "${var.tag_name} - subnet 1"
    }
}

# Subnet 2
resource "aws_subnet" "infra1_subnet2" {
    vpc_id = aws_vpc.infra1.id
    cidr_block = "10.0.2.0/24"
    availability_zone = "${var.region}b"

    tags = {
        Name = "${var.tag_name} - subnet 2"
    }
}


# 5 Association Subnet / Route table
resource "aws_route_table_association" "infra1_subnet1" {
    subnet_id      = aws_subnet.infra1_subnet1.id
    route_table_id = aws_route_table.infra1.id
}

resource "aws_route_table_association" "infra1_subnet2" {
    subnet_id      = aws_subnet.infra1_subnet2.id
    route_table_id = aws_route_table.infra1.id
}


# 6 Security Group (Firewall rules) for SSH and HTTP
resource "aws_security_group" "infra1" {
    name        = "allow_web"
    description = "Allow HTTP and SSH"
    vpc_id      = aws_vpc.infra1.id

    ingress {
        description      = "HTTP from anywhere"
        from_port        = 80
        to_port          = 80
        protocol         = "tcp"
        cidr_blocks      = ["0.0.0.0/0"]
    }
    ingress {
        description      = "SSH from anywhere"
        from_port        = 22
        to_port          = 22
        protocol         = "tcp"
        cidr_blocks      = ["0.0.0.0/0"]
    }

    egress {
        from_port        = 0
        to_port          = 0
        protocol         = "-1" # n'importe quel protocole
        cidr_blocks      = ["0.0.0.0/0"]
    }

    tags = {
         Name = var.tag_name
    }
}

# 7 Elastic Network Interface (ENI)
resource "aws_network_interface" "infra1" {
    subnet_id           = aws_subnet.infra1_subnet1.id
    private_ips         = [var.private_ip]
    security_groups     = [aws_security_group.infra1.id]

    tags = {
         Name = var.tag_name
    }
}

# 8 Elastic IP
resource "aws_eip" "infra1" {
    vpc                         = true
    network_interface           = aws_network_interface.infra1.id
    associate_with_private_ip   = var.private_ip
    depends_on                  = [aws_internet_gateway.infra1]

    tags = {
         Name = var.tag_name
    }
}


# 9 EC2 Web Server
resource "aws_instance" "infra1" {
    ami = var.ami_id
    instance_type = "t2.micro"
    #availability_zone = "${var.region}a"
    #key_name = "infra1_chris"
    key_name = aws_key_pair.infra1.key_name

    network_interface {
        device_index = 0
        network_interface_id = aws_network_interface.infra1.id
    }

    # script à exécuter au lancement initial de la vm
    user_data = file("install_apache.sh")
    
    tags = {
         Name = var.tag_name
    }
}

# TP1
# 1a
resource "aws_key_pair" "infra1" {
  key_name   = "kp-infra1"
  public_key = file("infra1.pub")
}

# 2
resource "aws_instance" "trotro" {
    ami = var.ami_id
    instance_type = "t2.micro"
    subnet_id = aws_subnet.infra1_subnet1.id
    security_groups = [aws_security_group.trotro.id]
    tags = { Name = "trotro" }
}

resource "aws_security_group" "trotro" {
    name        = "allow_icmp"
    description = "Allow ICMP"
    vpc_id      = aws_vpc.infra1.id

    ingress {
        description      = "ICMP from target SG"
        from_port        = -1
        to_port          = -1
        protocol         = "icmp"
        security_groups  = [aws_security_group.infra1.id]
    }

    egress {
        from_port        = 0
        to_port          = 0
        protocol         = "-1" # n'importe quel protocole
        cidr_blocks      = ["0.0.0.0/0"]
    }

    tags = {
         Name = "trotro"
    }
}




# 10 DB server (instance RDS)


