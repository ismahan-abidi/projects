provider "aws" {
  region                   = "eu-west-2" // London
  shared_config_files      = ["C:/Users/chris/.aws/config"]
  shared_credentials_files = ["C:/Users/chris/.aws/credentials"]
  profile                  = "ajc"
}