output "eip_public_dns" {
    value = aws_eip.infra1.public_dns
}

output "trotro_private_ip" {
    value = aws_instance.trotro.private_ip
}